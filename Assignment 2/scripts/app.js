(function(app){
    
    $("body").on("click", function(event){
        $(".search-results-tab").hide();
        event.stopPropagation();
    });
    
    $(".search-box").on("click", function(){
        $(".search-results-tab").hide();
        this.querySelector(".search-results-tab").style.display = "block";
        event.stopPropagation();
    });
    
    $(".search-result").on("click", function(){
        $(this).closest(".search-box").children(".val").html($(this).html());
        $(".search-results-tab").hide();
        event.stopPropagation();
    });
    
    $(".search-input").on("keyup", function(e){
        var result = $(this).closest(".search-box").find(".search-result");
        $(this).closest(".search-box").find(".search-result").show();
        var searchText = $(this).val();
        searchText = searchText.trim();
        //console.log(searchText);
        if(searchText || searchText !== "") {
            for(var index in result) {
                if(result[index].innerHTML && result[index].innerHTML.toLowerCase().indexOf(searchText) === -1) {
                    $(result[index]).hide();
                }
            }
        }
    });
})(app);