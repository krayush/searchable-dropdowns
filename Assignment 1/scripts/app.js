
(function(app){
    app.init = function() {
        var userData = {};
        localStorage.setItem("userData", JSON.stringify(userData));
        app.bindFormSubmit();
    };
    
    app.bindFormSubmit = function() {
        document.querySelector("#user-form").addEventListener("submit", app.formSubmitHandler);
        document.querySelector("#reset-button").addEventListener("click", app.clearErrorSections);
        app.addBlurEffect("#password", app.util.validateLength, app.errorConfig.passwordLengthError);
        app.addBlurEffect("#username", app.util.validateAfterTrim, app.errorConfig.usernameError);
        app.addBlurEffect("#phone", app.util.validateToDigits, app.errorConfig.phoneNoError);
        app.addBlurEffect("#zipcode", app.util.validateToDigits, app.errorConfig.zipCodeError);
        app.addBlurEffect("#userdate", app.util.validDate, app.errorConfig.dateError);
        app.addBlurEffect("#email", app.util.validateEmail, app.errorConfig.emailError);
    };
    
    app.addBlurEffect = function(selector, eval, errorMessage) {
        document.querySelector(selector).addEventListener("blur", function(){
            app.checkValidity(selector, eval, errorMessage);
        });
    };
    
    app.formSubmitHandler = function(eventObject) {
        var password = app.checkValidity("#password", app.util.validateLength, app.errorConfig.passwordLengthError);
        var username = app.checkValidity("#username", app.util.validateAfterTrim, app.errorConfig.usernameError);
        var phone = app.checkValidity("#phone", app.util.validateToDigits, app.errorConfig.phoneNoError);
        var zipcode = app.checkValidity("#zipcode", app.util.validateToDigits, app.errorConfig.zipCodeError);
        var userdate = app.checkValidity("#userdate", app.util.validDate, app.errorConfig.dateError);
        var email = app.checkValidity("#email", app.util.validateEmail, app.errorConfig.emailError);
        
        if(password && username && phone && zipcode && userdate && email) {
            var userData = JSON.parse(localStorage.getItem("userData"));
            var element = document.querySelector(".main-status");
            if(userData[email]) {
                element.classList.add("main-error");
                element.innerHTML = "Duplicate Email Identified";
                element.classList.remove("success");
            } else {
                userData[email] = {
                    email: email,
                    password:password,
                    username: username,
                    zipcode: zipcode,
                    phone: phone,
                    userdate:userdate
                }
                localStorage.setItem("userData", JSON.stringify(userData));
                element.classList.remove("main-error");
                element.innerHTML = "Insertion Successful";
                element.classList.add("success");
            }
        }
        app.disableSubmit(eventObject);
    };
    
    app.disableSubmit = function(eventObject) {
        if (eventObject.preventDefault) {
            eventObject.preventDefault();
        } else if (window.event) { // Check added for IE
            window.event.returnValue = false;
        }
    };
    
    app.checkValidity = function(selector, validateFunction, errorMessage) {
        var element = document.querySelector(selector);
        var value = element.value;
        //debugger;
        var errorDiv = element.parentNode.parentNode.querySelector(".error-div");
        if(!validateFunction(value)) {
            //element.parentNode(".row")
            if(!errorDiv) {
                var newErrorDiv = document.createElement("div");
                newErrorDiv.classList.add("error-div");
                newErrorDiv.innerHTML = errorMessage;
                var arrowDiv = document.createElement("div");
                arrowDiv.classList.add("arrow-left");
                element.parentNode.parentNode.appendChild(newErrorDiv);
                element.parentNode.parentNode.appendChild(arrowDiv);
            }
            return false;
        } else {
            if(errorDiv) {
                errorDiv.remove();
                element.parentNode.parentNode.querySelector(".arrow-left").remove();
            }
            return value;
        }
    };
    
    app.clearErrorSections = function () {
        var errorDivs = document.querySelectorAll(".error-div");
        for(var index in errorDivs) {
            if(errorDivs[index].remove) {
                errorDivs[index].remove();
                document.querySelector(".arrow-left").remove();
            }
        }
    };
    
    app.init();
})(app);