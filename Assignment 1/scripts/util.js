var app = {};
(function(app){
    app.util = {};
    
    if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, ''); 
        }
    }
    
    app.util.validateAfterTrim = function(value) {
        if(value.trim()) {
            return true;
        }
    };
    
    app.util.validateLength = function(value) {
        if(value.trim().length >= 6) {
            return true;
        }
    };
    
    app.util.validateEmail = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    };
    
    app.util.validateToDigits = function(val) {
        return /^\d+$/.test(val);
    };
    
    app.util.validateToDigits = function(val) {
        return /^\d+$/.test(val);
    };
    
    app.util.validDate = function(dateValue) {
        var bits = dateValue.split('/');
        var d = new Date(bits[2], bits[1] - 1, bits[0]);
        return d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[0]);
    };
})(app);