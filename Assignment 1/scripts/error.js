(function(app){
    app.errorConfig = {
        passwordLengthError: "Password length should be 6 characters",
        usernameError: "Username is required",
        dateError: "Please enter a valid date",
        zipCodeError: "Zip Code should only contain digits",
        phoneNoError: "Phone No. should only contain digits",
        emailError: "Please enter a valid email"
    };
})(app);